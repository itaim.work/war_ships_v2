using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Simple_Node : MonoBehaviour
{
    [SerializeField] private AI_Controller AI_Controller_Script;
    [SerializeField] private Material NodeMat;
    [SerializeField] private bool Hit;
    [SerializeField] private Color DefColor;

    // Start is called before the first frame update
    void Start()
    {
        AI_Controller_Script = GameObject.Find("AI_Controller").GetComponent<AI_Controller>();
        NodeMat = gameObject.GetComponent<Renderer>().material;
        DefColor = NodeMat.color;
    }

    // Update is called once per frame
    void Update()
    {
        
    }



    private void OnMouseDown()
    {
        AI_Controller_Script.RegisterHit(this);
    }

    public void SetColor(Color color)
    {
        NodeMat.color = color;
    }

    public void SetHit(bool value)
    {
        this.Hit = value;
    }
}
