using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public enum State
    {
        start,
        playerTurn,
        AI_Turn,
        End
    }

    [SerializeField] private State ActiveState;
    [SerializeField] private PlayerController PlayerControllerScript;
    [SerializeField] private AI_Controller AI_Controller_Script;
    [SerializeField] private UI_Manager UI_Manager_Script;

    // Start is called before the first frame update
    void Start()
    {
        UI_Manager_Script = GameObject.Find("Canvas").GetComponent<UI_Manager>();
        AI_Controller_Script = GameObject.Find("AI_Controller").GetComponent<AI_Controller>();
        PlayerControllerScript = GameObject.Find("PlayerController").GetComponent<PlayerController>();

    }

    // Update is called once per frame
    void Update()
    {
        switch (ActiveState)
        {
            case State.start:
                if (PlayerControllerScript.GetCounter() >= 1)
                {
                    ActiveState = (State)Random.Range(1, 3);
                    AI_Controller_Script.FillPlayermap();
                }
                break;
            case State.playerTurn:
                break;
            case State.AI_Turn:
                AI_Controller_Script.Sod();
                UI_Manager_Script.SetTurnText("Turn: Player");
                ActiveState = State.playerTurn;
                break;
            case State.End:
                break;
        }
    }

    public void SetActiveState(State NewState)
    {
        this.ActiveState = NewState;
    }

    public State GetActiveState()
    {
        return this.ActiveState;
    }
}

