using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI_Manager : MonoBehaviour
{
    [SerializeField] private PlayerController PlayerControllerScript;
    [SerializeField] private Text TurnText;
    [SerializeField] private GameObject WinMenu, LoseMenu , PauseMenu;
    // Start is called before the first frame update
    void Start()
    {
        PlayerControllerScript = GameObject.Find("PlayerController").GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (PauseMenu.activeSelf)
            {
                ResumeGame();
            }
            else
            {
                PauseGame();
            }
        }
    }


    public void SetTurnText(string text)
    {
        TurnText.text = text;
    }

    public void BuildShip(GameObject ship)
    {
        PlayerControllerScript.enableShip(ship);
    }
    public void SetPlayerView()
    {
        Camera.main.transform.rotation = Quaternion.Euler(0, 0, 0);
    }
    public void Set_AI_View()
    {
        Camera.main.transform.rotation = Quaternion.Euler(60, 0, 0);
    }
    public void LoseGame()
    {
        Time.timeScale = 0;
        LoseMenu.SetActive(true);
    }
    public void WinGame()
    {
        Time.timeScale = 0;
        WinMenu.SetActive(true);
    }
    public void RestartGame()
    {
        Time.timeScale = 1;
        PauseMenu.SetActive(true);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void MainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
    public void PauseGame()
    {
        Time.timeScale = 0;
        PauseMenu.SetActive(true);
    }
    public void ResumeGame()
    {
        Time.timeScale = 1;
        PauseMenu.SetActive(false);

    }
}
