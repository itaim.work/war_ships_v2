using System;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public enum directions
    {
        down,   // 0
        up,     // 1
        left,   // 2
        right   // 3
    }

    [SerializeField] private directions ActiveDirection;
    [SerializeField] private Node[,] Nodes;
    [SerializeField] private int CurrentShipSize, counter;
    [SerializeField] private bool CanChooseNode, CanDeployShip;
    [SerializeField] private GameObject ActiveShip;

    public void enableShip(GameObject ship)
    {
        ActiveShip = ship;
        CanChooseNode = true;
        CurrentShipSize = ship.GetComponent<ShipCode>().GetSize();
    }

    // Start is called before the first frame update
    void Start()
    {
        int counter = 0;
        GameObject[] TempNodes;

        TempNodes = GameObject.FindGameObjectsWithTag("Node");

        Array.Sort(TempNodes, CompareByName);

        Nodes = new Node[10, 10];

        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                Nodes[i, j] = TempNodes[counter].GetComponent<Node>();
                counter++;
            }
        }

    }

    // Update is called once per frame
    void Update()
    {

    }

    public int test(String name)
    {

        int ValueToReturn = 5;
        int x, y;
        x = name[name.Length - 2] - '0';
        y = name[name.Length - 1] - '0';

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            ActiveDirection = directions.down;
            if (x + CurrentShipSize - 1 < 10)
            {
                ValueToReturn = TestRow(x, y, CurrentShipSize, directions.down) ? 2 : 1;

            }
            else
            {
                ValueToReturn = 1;
            }
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            ActiveDirection = directions.up;
            if (x - CurrentShipSize + 1 >= 0)
            {
                ValueToReturn = 1;
                ValueToReturn = TestRow(x, y, CurrentShipSize, directions.up) ? 2 : 1;
            }
            else
            {
                ValueToReturn = 1;
            }

        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            ActiveDirection = directions.left;
            if (y - CurrentShipSize + 1 >= 0)
            {
                ValueToReturn = TestRow(x, y, CurrentShipSize, directions.left) ? 2 : 1;
            }
            else
            {
                ValueToReturn = 1;
            }
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            ActiveDirection = directions.right;
            if (y + CurrentShipSize - 1 < 10)
            {
                ValueToReturn = ValueToReturn = TestRow(x, y, CurrentShipSize, directions.right) ? 2 : 1;

            }
            else
            {
                ValueToReturn = 1;
            }
        }

        return ValueToReturn;
    }

    public bool TestRow(int x, int y, int size, directions direction)
    {
        int index = (int)direction;

        if (size > 0)
        {


            switch (index)
            {
                case 0:
                    return Nodes[x, y].GetIsEmpty() && TestRow(x + 1, y, size - 1, direction);
                case 1:
                    return Nodes[x, y].GetIsEmpty() && TestRow(x - 1, y, size - 1, direction);
                case 2:
                    return Nodes[x, y].GetIsEmpty() && TestRow(x, y - 1, size - 1, direction);
                case 3:
                    return Nodes[x, y].GetIsEmpty() && TestRow(x, y + 1, size - 1, direction);

            }
        }
        return true;
    }
    public int CompareByName(GameObject a, GameObject b)
    {
        return a.transform.name.CompareTo(b.transform.name);
    }
    public Node[,] GetNodes()
    {
        return Nodes;
    }
    public int GetShipSize()
    {
        return CurrentShipSize;
    }
    public void CLS()
    {
        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                if (!Nodes[i, j].GetHit())
                {
                    Nodes[i, j].SetColor(Nodes[i, j].GetBaseColor());
                }
            }
        }
    }
    public bool GetCanChooseNode()
    {
        return CanChooseNode;
    }
    public void DeployShip(string name)
    {
        int x, y;
        x = name[name.Length - 2] - '0';
        y = name[name.Length - 1] - '0';
        ActiveShip.transform.position = Nodes[x, y].transform.localPosition;

        switch (ActiveDirection)
        {
            case directions.down:
                ActiveShip.transform.rotation = Quaternion.Euler(0, 90, 0);
                ActiveShip.transform.position = new Vector3(ActiveShip.transform.position.x, ActiveShip.transform.position.y + 1, ActiveShip.transform.position.z - CurrentShipSize  + 0.4f);

                break;
            case directions.up:
                ActiveShip.transform.rotation = Quaternion.Euler(0, 90, 0);
                ActiveShip.transform.position = new Vector3(ActiveShip.transform.position.x , ActiveShip.transform.position.y + 1, ActiveShip.transform.position.z + CurrentShipSize );

                break;
            case directions.left:
                ActiveShip.transform.rotation = Quaternion.Euler(0, 0, 0);
                ActiveShip.transform.position = new Vector3(ActiveShip.transform.position.x - CurrentShipSize, ActiveShip.transform.position.y + 1, ActiveShip.transform.position.z);

                break;
            case directions.right:
                ActiveShip.transform.rotation = Quaternion.Euler(0, 0, 0);
                ActiveShip.transform.position = new Vector3(ActiveShip.transform.position.x + CurrentShipSize, ActiveShip.transform.position.y + 1, ActiveShip.transform.position.z);
                break;
        }
        CanDeployShip = true;
    }
    public void SetShip(string name)
    {
        if (CanDeployShip)
        {
            int x, y, index;
            index = (int)ActiveDirection;
            x = name[name.Length - 2] - '0';
            y = name[name.Length - 1] - '0';
            CanChooseNode = false;
            CanDeployShip = false;
            SetRow(x, y, CurrentShipSize, index);
            counter++;
        }
    }
    public void SetRow(int x, int y, int size, int index)
    {
        if (size > 0)
        {
            switch (index)
            {
                case 0:
                    Nodes[x, y].SetIsEmpty(false);
                    SetRow(x + 1, y, size - 1, index);
                    break;
                case 1:
                    Nodes[x, y].SetIsEmpty(false);  
                    SetRow(x - 1, y, size - 1, index);
                    break;
                case 2:
                    Nodes[x, y].SetIsEmpty(false);
                    SetRow(x, y - 1, size - 1, index);
                    break;
                case 3:
                    Nodes[x, y].SetIsEmpty(false);
                    SetRow(x, y + 1, size - 1, index);
                    break;
            }
        }

    }
    public void ResetShip()
    {
        ActiveShip.transform.position = ActiveShip.GetComponent<ShipCode>().getLastPos();
        ActiveShip.transform.rotation = Quaternion.identity;
    }

    public void NodeHit(int x, int y, Color color)
    {
        Nodes[x, y].NodeHit(color);
    }

    public int GetCounter()
    {
        return this.counter;
    }

    public void SetCanDeployShip(bool value)
    {
        this.CanDeployShip = value;
    }
}

