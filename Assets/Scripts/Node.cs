using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{

    [SerializeField] private Color BaseColor;
    [SerializeField] private Material NodeMaterial;

    [SerializeField] private PlayerController PlayerControllerScript;

    [SerializeField] private bool IsEmpty = true;
    [SerializeField] private bool Hit = false, IsDestroyed;


    // Start is called before the first frame update
    void Start()
    {
        NodeMaterial = gameObject.GetComponent<Renderer>().material;
        BaseColor = NodeMaterial.color;
        PlayerControllerScript = GameObject.Find("PlayerController").GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {

    }


    private void OnMouseOver()
    {

        if (PlayerControllerScript.GetCanChooseNode() && IsEmpty)
        {
            if (NodeMaterial.color == BaseColor)
            {
                SetColor(Color.cyan);
            }

            int result = PlayerControllerScript.test(transform.name);

            if (result == 1)
            {
                SetColor(Color.red);
                PlayerControllerScript.SetCanDeployShip(false);
            }
            else if (result == 2)
            {
                PlayerControllerScript.DeployShip(transform.name);
                PlayerControllerScript.SetCanDeployShip(true);

                SetColor(Color.green);
            }


        }
    }

    private void OnMouseExit()
    {
        PlayerControllerScript.CLS();
    }

    private void OnMouseDown()
    {
        print("Pressed");
        PlayerControllerScript.SetShip(this.name);
    }

    public Color GetBaseColor()
    {
        return BaseColor;
    }

    public void SetColor(Color color)
    {
        NodeMaterial.color = color;

    }
    public void SetIsEmpty(bool value)
    {
        IsEmpty = value;
    }
    public bool GetIsEmpty()
    {
        return IsEmpty;
    }


    public bool GetHit()
    {
        return Hit;
    }

    public void SetHit(bool value)
    {
        this.Hit = value;
    }

    public void NodeHit(Color color ) 
    {
        SetColor(color);
        SetHit(true);
        transform.localScale = new Vector3(1, 3, 1);
    }
}


