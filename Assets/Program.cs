﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp11
{
    class Program
    {
        static void Main(string[] args)
        {
            //  ריק - 0
            //  יש ספינה - 1
            //  אומר פגיעה - 3
            //  אומר החטאה - 4
            int[,] playerBoard = new int[10, 10]; // הלוח עם הצוללות של השחקן
            int[,] playerSecondBoard = new int[10, 10]; // הלוח ניסיונות של המחשב
            int[,] aiBoard = new int[10, 10]; // הלוח עם הצוללות של המחשב
            int[,] aiSecondBoard = new int[10, 10]; // הלוח ניסיונות של השחקן
            int[,] helpBoard = new int[10, 10]; // מערך ריק         
            Random rnd = new Random();
            PlaceSubmarine(playerBoard, 5, playerSecondBoard);
            FillWithNum(playerSecondBoard, 2, 1);
            Compare(playerSecondBoard, playerBoard);
            Ai(aiSecondBoard, 5);
            FillWithNum(aiSecondBoard, 2, 1);
            Compare(aiSecondBoard, aiBoard);
            PlaceSubmarine(playerBoard, 4, playerSecondBoard);
            FillWithNum(playerSecondBoard, 2, 1);
            Compare(playerSecondBoard, playerBoard);
            Ai(aiSecondBoard, 4);
            FillWithNum(aiSecondBoard, 2, 1);
            Compare(aiSecondBoard, aiBoard);
            PlaceSubmarine(playerBoard, 3, playerSecondBoard);
            FillWithNum(playerSecondBoard, 2, 1);
            Compare(playerSecondBoard, playerBoard);
            Ai(aiSecondBoard, 3);
            FillWithNum(aiSecondBoard, 2, 1);
            Compare(aiSecondBoard, aiBoard);
            PlaceSubmarine(playerBoard, 3, playerSecondBoard);
            FillWithNum(playerSecondBoard, 2, 1);
            Compare(playerSecondBoard, playerBoard);
            Ai(aiSecondBoard, 3);
            FillWithNum(aiSecondBoard, 2, 1);
            Compare(aiSecondBoard, aiBoard);
            PlaceSubmarine(playerBoard, 2, playerSecondBoard);
            FillWithNum(playerSecondBoard, 2, 1);
            Compare(playerSecondBoard, playerBoard);
            Ai(aiSecondBoard, 2);
            FillWithNum(aiSecondBoard, 2, 1);
            Compare(aiSecondBoard, aiBoard);
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    aiSecondBoard[i, j] = aiBoard[i, j];
                    playerSecondBoard[i, j] = playerBoard[i, j];
                }
            }
            bool loPaga = true;
            string ans = "Didnt hit a ship befor";
            int computerRow = 0;
            int computerLine = 0;
            int count = 0;
            while (!CheckEndGame(aiBoard) && !CheckEndGame(playerSecondBoard)) // בנתיים המערך הזה
            {
                Console.Clear();
                PrintBoards(playerBoard, aiBoard);
                Console.WriteLine("This is your turn");
                Console.WriteLine();
                Console.WriteLine("Pick a row you want to shot");
                string row = Console.ReadLine();
                while (!CheckNumber(row))
                {
                    Console.Clear();
                    PrintBoards(playerBoard, aiBoard);
                    Console.WriteLine("This is your turn");
                    Console.WriteLine();
                    Console.WriteLine("Pick a row you want to shot");
                    row = Console.ReadLine();
                }
                Console.WriteLine("Pick a line you want to shot");
                string line = Console.ReadLine();
                while (!CheckNumber(line))
                {
                    Console.Clear();
                    PrintBoards(playerBoard, aiBoard);
                    Console.WriteLine("This is your turn");
                    Console.WriteLine();
                    Console.WriteLine("Pick a row you want to shot");
                    Console.WriteLine(row);
                    Console.WriteLine("Pick a line you want to shot");
                    line = Console.ReadLine();
                }
                int newRow = int.Parse(row) - 1;
                int newLine = int.Parse(line) - 1;
                if (aiBoard[newRow, newLine] == 0) // אם השחקן החטיא
                {
                    loPaga = true;
                    aiBoard[newRow, newLine] = 4;
                    aiSecondBoard[newRow, newLine] = 4;
                    Console.Clear();
                    PrintBoards(playerBoard, aiBoard);
                    Console.WriteLine("You miss");
                }
                else if (aiBoard[newRow, newLine] == 1) // אם השחקן פגע
                {
                    if (CheckDestroyedShip(aiSecondBoard, newRow, newLine))
                    {
                        loPaga = true;
                        Console.Clear();
                        PrintBoards(playerBoard, aiBoard);
                        Console.WriteLine("You hit a destroyed ship");
                    }
                    else
                    {
                        loPaga = false;
                        aiBoard[newRow, newLine] = 3;
                        aiSecondBoard[newRow, newLine] = 3;
                        Console.Clear();
                        PrintBoards(playerBoard, aiBoard);
                        if (CheckDestroyedShip(aiSecondBoard, newRow, newLine))
                        {
                            Console.WriteLine("You destroy a ship");
                        }
                        else
                        {
                            Console.WriteLine("You hit a ship");
                        }
                    }
                }
                else
                {
                    loPaga = true;
                    Console.Clear();
                    PrintBoards(playerBoard, aiBoard);
                    Console.WriteLine("You hit here befor");
                }
                Console.WriteLine();
                Console.WriteLine("Pess any key");
                ConsoleKeyInfo temp1 = Console.ReadKey(true);
                if (!CheckEndGame(aiSecondBoard) && loPaga)
                {
                    if (ans == "Didnt hit a ship befor")
                    {
                        int randomPlace = RandomNumber(playerSecondBoard);
                        for (int i = 0; i < playerSecondBoard.GetLength(0); i++) // המחשב בוחר מיקום  מהמקומת הרקים לזרוק בו פצצה
                        {
                            for (int j = 0; j < playerSecondBoard.GetLength(1); j++)
                            {
                                if (playerSecondBoard[i, j] == 0 || playerSecondBoard[i, j] == 1)
                                {
                                    randomPlace--;
                                    if (randomPlace == 0)
                                    {
                                        computerRow = i;
                                        computerLine = j;
                                        i = playerSecondBoard.GetLength(0);
                                        j = playerSecondBoard.GetLength(1);
                                    }
                                }
                            }
                        }
                        Console.Clear();
                        Console.WriteLine("This is the computers turn");
                        Console.WriteLine();
                        if (playerBoard[computerRow, computerLine] == 0) // אם המחשב החטיא
                        {
                            loPaga = true;
                            playerSecondBoard[computerRow, computerLine] = 4;
                            playerBoard[computerRow, computerLine] = 4;
                            Console.Clear();
                            PrintBoards(playerBoard, aiBoard);
                            Console.WriteLine("The Computer miss");
                            Console.WriteLine();
                            Console.WriteLine("It hit in row number: " + (computerRow + 1) + " and line number: " + (computerLine + 1));
                            Console.WriteLine();
                            Console.WriteLine("Pess any key");
                            temp1 = Console.ReadKey(true);
                        }
                        if (playerBoard[computerRow, computerLine] == 1) // אם המחשב פגע
                        {
                            loPaga = false;
                            playerSecondBoard[computerRow, computerLine] = 3;
                            playerBoard[computerRow, computerLine] = 3;
                            Console.Clear();
                            PrintBoards(playerBoard, aiBoard);
                            Console.WriteLine("The Computer hit a ship");
                            Console.WriteLine();
                            Console.WriteLine("It hit in row number: " + (computerRow + 1) + " and line number: " + (computerLine + 1));
                            Console.WriteLine();
                            Console.WriteLine("Pess any key");
                            temp1 = Console.ReadKey(true);
                            ans = "Right";
                            count++;
                        }
                    }
                    else
                    {
                        loPaga = false;
                    }
                    while (!loPaga)
                    {
                        int newComputerRow = computerRow;
                        int newComputerLine = computerLine;
                        if (ans == "Right")
                        {
                            if (CheckCell(playerSecondBoard, newComputerRow, newComputerLine + (count - 1), ans) && playerSecondBoard[newComputerRow, newComputerLine + count] != 4)
                            {
                                newComputerLine += count;
                            }
                            else
                            {
                                ans = "Left";
                                count = 1;
                            }
                        }
                        if (ans == "Left")
                        {
                            if (CheckCell(playerSecondBoard, newComputerRow, newComputerLine - (count - 1), ans) && playerSecondBoard[newComputerRow, newComputerLine - count] != 4)
                            {
                                newComputerLine -= count;
                            }
                            else
                            {
                                ans = "Up";
                                count = 1;
                            }
                        }
                        if (ans == "Up")
                        {
                            if (CheckCell(playerSecondBoard, newComputerRow - (count - 1), newComputerLine, ans) && playerSecondBoard[newComputerRow - count, newComputerLine] != 4)
                            {
                                newComputerRow -= count;
                            }
                            else
                            {
                                ans = "Down";
                                count = 1;
                            }
                        }
                        if (ans == "Down")
                        {
                            if (CheckCell(playerSecondBoard, newComputerRow + (count - 1), newComputerLine, ans) && playerSecondBoard[newComputerRow + count, newComputerLine + count] != 4)
                            {
                                newComputerRow += count;
                            }
                            else
                            {
                                count = 1;
                            }
                        }
                        Console.Clear();
                        if (playerBoard[newComputerRow, newComputerLine] == 0) // אם המחשב החטיא
                        {
                            loPaga = true;
                            count = 1;
                            playerSecondBoard[newComputerRow, newComputerLine] = 4;
                            playerBoard[newComputerRow, newComputerLine] = 4;
                            Console.Clear();
                            PrintBoards(playerBoard, aiBoard);
                            Console.WriteLine("The Computer miss");
                            Console.WriteLine();
                            Console.WriteLine("It hit in row number: " + (newComputerRow + 1) + " and line number: " + (newComputerLine + 1));
                            Console.WriteLine();
                            Console.WriteLine("Pess any key");
                            temp1 = Console.ReadKey(true);
                            if (ans == "Right")
                            {
                                ans = "Left";
                            }
                            else if (ans == "Left")
                            {
                                ans = "Up";
                            }
                            else if (ans == "Up")
                            {
                                ans = "Down";
                            }
                            else if (ans == "Down")
                            {
                                ans = "Right";
                            }
                        }
                        if (playerBoard[newComputerRow, newComputerLine] == 1) // אם המחשב פגע
                        {
                            loPaga = false;
                            playerSecondBoard[newComputerRow, newComputerLine] = 3;
                            playerBoard[newComputerRow, newComputerLine] = 3;
                            if (CheckDestroyedShip(playerSecondBoard, newComputerRow, newComputerLine))
                            {
                                Console.Clear();
                                PrintBoards(playerBoard, aiBoard);
                                Console.WriteLine("The Computer destroy a ship");
                                Console.WriteLine();
                                Console.WriteLine("It hit in row number: " + (newComputerRow + 1) + " and line number: " + (newComputerLine + 1));
                                Console.WriteLine();
                                Console.WriteLine("Pess any key");
                                temp1 = Console.ReadKey(true);
                                ans = "Didnt hit a ship befor";
                                count = 0;
                                FillWithNum(playerSecondBoard, 4, 3);
                                int randomPlace = RandomNumber(playerSecondBoard);
                                for (int i = 0; i < playerSecondBoard.GetLength(0); i++) // המחשב בוחר מיקום  מהמקומת הרקים לזרוק בו פצצה
                                {
                                    for (int j = 0; j < playerSecondBoard.GetLength(1); j++)
                                    {
                                        if (playerSecondBoard[i, j] == 0 || playerSecondBoard[i, j] == 1)
                                        {
                                            randomPlace--;
                                            if (randomPlace == 0)
                                            {
                                                computerRow = i;
                                                computerLine = j;
                                                i = playerSecondBoard.GetLength(0);
                                                j = playerSecondBoard.GetLength(1);
                                            }
                                        }
                                    }
                                }
                                Console.Clear();
                                Console.WriteLine("This is the computers turn");
                                Console.WriteLine();
                                if (playerBoard[computerRow, computerLine] == 0) // אם המחשב החטיא
                                {
                                    loPaga = true;
                                    playerSecondBoard[computerRow, computerLine] = 4;
                                    playerBoard[computerRow, computerLine] = 4;
                                    Console.Clear();
                                    PrintBoards(playerBoard, aiBoard);
                                    Console.WriteLine("The Computer miss");
                                    Console.WriteLine();
                                    Console.WriteLine("It hit in row number: " + (computerRow + 1) + " and line number: " + (computerLine + 1));
                                    Console.WriteLine();
                                    Console.WriteLine("Pess any key");
                                    temp1 = Console.ReadKey(true);
                                }
                                if (playerBoard[computerRow, computerLine] == 1) // אם המחשב פגע
                                {
                                    loPaga = false;
                                    playerSecondBoard[computerRow, computerLine] = 3;
                                    playerBoard[computerRow, computerLine] = 3;
                                    Console.Clear();
                                    PrintBoards(playerBoard, aiBoard);
                                    Console.WriteLine("The Computer hit a ship");
                                    Console.WriteLine();
                                    Console.WriteLine("It hit in row number: " + (computerRow + 1) + " and line number: " + (computerLine + 1));
                                    Console.WriteLine();
                                    Console.WriteLine("Pess any key");
                                    temp1 = Console.ReadKey(true);
                                    ans = "Right";
                                    count++;
                                }
                            }
                            else
                            {
                                count++;
                                Console.Clear();
                                PrintBoards(playerBoard, aiBoard);
                                Console.WriteLine("The Computer hit a ship");
                                Console.WriteLine();
                                Console.WriteLine("It hit in row number: " + (newComputerRow + 1) + " and line number: " + (newComputerLine + 1));
                                Console.WriteLine();
                                Console.WriteLine("Pess any key");
                                temp1 = Console.ReadKey(true);
                            }
                        }
                    }
                }
            }
            Console.Clear();
            if (CheckEndGame(aiBoard))
            {
                Console.WriteLine("You win!");
            }
            else
            {
                Console.WriteLine("The computer win!");
            }
        }
        public static void PrintBoards(int[,] playerBoard, int[,] aiBoard)
        {
            Console.Clear();
            Console.WriteLine("This is the computer's board:");
            Console.WriteLine();
            for (int i = 0; i < aiBoard.GetLength(0); i++)
            {
                for (int j = 0; j < aiBoard.GetLength(1); j++)
                {
                    if (aiBoard[i, j] == 0 || aiBoard[i, j] == 1)
                    {
                        Console.Write(" 0");
                    }
                    //if (aiBoard[i, j] == 1)
                    //{
                    //    Console.ForegroundColor = ConsoleColor.Blue;
                    //    Console.Write(" 0");
                    //    Console.ResetColor();
                    //}
                    if (aiBoard[i, j] == 3)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write(" 0");
                        Console.ResetColor();
                    }
                    if (aiBoard[i, j] == 4)
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write(" 0");
                        Console.ResetColor();
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.WriteLine("This is you'r board:");
            Console.WriteLine();
            for (int i = 0; i < playerBoard.GetLength(0); i++)
            {
                for (int j = 0; j < playerBoard.GetLength(1); j++)
                {
                    if (playerBoard[i, j] == 0)
                    {
                        Console.Write(" 0");
                    }
                    if (playerBoard[i, j] == 1)
                    {
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.Write(" 0");
                        Console.ResetColor();
                    }
                    if (playerBoard[i, j] == 3)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write(" 0");
                        Console.ResetColor();
                    }
                    if (playerBoard[i, j] == 4)
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write(" 0");
                        Console.ResetColor();
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        } // הפקודה מדפיסה את הלוחות שהשחקן רואה
        public static void PrintPlayerBoard(int[,] playerBoard)
        {
            Console.Clear();
            Console.WriteLine("This is you'r board:");
            Console.WriteLine();
            for (int i = 0; i < playerBoard.GetLength(0); i++)
            {
                for (int j = 0; j < playerBoard.GetLength(1); j++)
                {
                    if (playerBoard[i, j] == 1)
                    {
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.Write(" 0");
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.Write(" 0");
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        } // הפקודה מדפיסה את הלוח של השחקן
        public static void PrintColor(int[,] arr, int x, int y)
        {
            Console.Clear();
            Console.WriteLine("This is you'r choice on the board:");
            Console.WriteLine();
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    if (arr[i, j] == 1)
                    {
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.Write(" 0");
                        Console.ResetColor();
                    }
                    else if (i == x && j == y)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write(" 0");
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.Write(" 0");
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        } // הפעולה מדגישה את המיקום העתידי לצוללת
        public static void PlaceSubmarine(int[,] arr, int length, int[,] secondArr)
        {
            Console.Clear();
            string ans = "yes";
            bool temp = true;
            string XstartingPosition = "";
            string YstartingPosition = "";
            bool temp1 = true;
            string newDirection = "";
            int NewXstartingPosition = 0;
            int NewYstartingPosition = 0;
            while (ans == "yes")
            {
                temp1 = true;
                while (temp1)
                {
                    Console.Clear();
                    PrintPlayerBoard(arr);
                    Console.WriteLine("Write you'r starting position: row");
                    XstartingPosition = Console.ReadLine();
                    while (!CheckNumber(XstartingPosition))
                    {
                        Console.Clear();
                        PrintPlayerBoard(arr);
                        Console.WriteLine("Write you'r starting position: row");
                        XstartingPosition = Console.ReadLine();
                    }
                    Console.WriteLine("Write you'r starting position: line");
                    YstartingPosition = Console.ReadLine();
                    while (!CheckNumber(YstartingPosition))
                    {
                        Console.Clear();
                        PrintPlayerBoard(arr);
                        Console.WriteLine("Write you'r starting position: row");
                        Console.WriteLine(XstartingPosition);
                        Console.WriteLine("Write you'r starting position: line");
                        YstartingPosition = Console.ReadLine();
                    }
                    NewXstartingPosition = int.Parse(XstartingPosition) - 1;
                    NewYstartingPosition = int.Parse(YstartingPosition) - 1;
                    if (secondArr[NewXstartingPosition, NewYstartingPosition] == 0)
                    {
                        temp1 = false;
                    }
                }
                Console.Clear();
                PrintColor(arr, NewXstartingPosition, NewYstartingPosition);
                Console.WriteLine("Press the direction of the ship");
                ConsoleKeyInfo direction = Console.ReadKey(true);
                newDirection = ConvetDirectionToString(direction);
                ans = "no";
                temp = CheckZeros(secondArr, NewXstartingPosition, NewYstartingPosition, newDirection, length);
                while (!temp)
                {
                    while ((ans != "yes" && ans != "no") || ans == "no")
                    {
                        Console.Clear();
                        PrintColor(arr, NewXstartingPosition, NewYstartingPosition);
                        Console.WriteLine(" yes = for new starting position");
                        Console.WriteLine(" no = for a diffrent direction");
                        ans = Console.ReadLine();
                        if (ans == "no")
                        {
                            Console.Clear();
                            PrintColor(arr, NewXstartingPosition, NewYstartingPosition);
                            Console.WriteLine("Press you'r new direction");
                            direction = Console.ReadKey(true);
                            newDirection = ConvetDirectionToString(direction);
                            temp = CheckZeros(secondArr, NewXstartingPosition, NewYstartingPosition, newDirection, length);
                            if (temp)
                            {
                                ans = "yes";
                            }
                        }
                    }
                    if (ans == "yes")
                    {
                        break;
                    }
                }
                if (temp)
                {
                    ans = "no";
                }
            }
            if (newDirection == "Right")
            {
                for (int i = 0; i < length; i++)
                {
                    secondArr[NewXstartingPosition, NewYstartingPosition + i] = 1;
                }
            }
            if (newDirection == "Left")
            {
                for (int i = 0; i < length; i++)
                {
                    secondArr[NewXstartingPosition, NewYstartingPosition - i] = 1;
                }
            }
            if (newDirection == "Up")
            {
                for (int i = 0; i < length; i++)
                {
                    secondArr[NewXstartingPosition - i, NewYstartingPosition] = 1;
                }
            }
            if (newDirection == "Down")
            {
                for (int i = 0; i < length; i++)
                {
                    secondArr[NewXstartingPosition + i, NewYstartingPosition] = 1;
                }
            }
        } // הפעולה מקבלת את אורך הצוללת ואת הלוח ומכניסה את הצוללת לתוך הלוח   
        public static bool CheckDirection(int[,] arr, int x, int y, string direction, int length)
        {
            if (direction == "Right")
            {
                if (y + length > 10)
                {
                    return false;
                }
            }
            if (direction == "Left")
            {
                if (y - length < -1)
                {
                    return false;
                }
            }
            if (direction == "Up")
            {
                if (x - length < -1)
                {
                    return false;
                }
            }
            if (direction == "Down")
            {
                if (x + length > 10)
                {
                    return false;
                }
            }
            return true;
        } // הפעולה בודקת שאכן הכיוון שהשחקן רוצה להניח את הצוללת תקין 
        public static bool CheckZeros(int[,] arr, int x, int y, string direction, int length)
        {
            if (CheckDirection(arr, x, y, direction, length))
            {
                if (direction == "Right")
                {
                    for (int i = 0; i < length; i++)
                    {
                        if (arr[x, y + i] != 0)
                        {
                            return false;
                        }
                    }
                }
                if (direction == "Left")
                {
                    for (int i = 0; i < length; i++)
                    {
                        if (arr[x, y - i] != 0)
                        {
                            return false;
                        }
                    }
                }
                if (direction == "Up")
                {
                    for (int i = 0; i < length; i++)
                    {
                        if (arr[x - i, y] != 0)
                        {
                            return false;
                        }
                    }
                }
                if (direction == "Down")
                {
                    for (int i = 0; i < length; i++)
                    {
                        if (arr[x + i, y] != 0)
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
            return false;
        } // הפעולה בודקת אם כל התאים שהשחקן בחר לשים בהם צוללת ריקים או לא
        public static void FillWithNum(int[,] arr, int num, int b1)
        {
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    if (arr[i, j] == 0 && j > 0 && j < 9 && i > 0 && i < 9)
                    {
                        if (arr[i + 1, j] == b1 || arr[i + 1, j - 1] == b1 || arr[i + 1, j + 1] == b1 || arr[i, j + 1] == b1 || arr[i, j - 1] == b1 || arr[i - 1, j] == b1 || arr[i - 1, j + 1] == b1 || arr[i - 1, j - 1] == b1)
                        {
                            arr[i, j] = num;
                        }
                    }
                    if (arr[i, j] == 0 && i == 0 && j > 0 && j < 9)
                    {
                        if (arr[1, j] == b1 || arr[1, j + 1] == b1 || arr[1, j - 1] == b1 || arr[0, j + 1] == b1 || arr[0, j - 1] == b1)
                        {
                            arr[i, j] = num;
                        }
                    }
                    if (arr[i, j] == 0 && i == 9 && j > 0 && j < 9)
                    {
                        if (arr[8, j] == b1 || arr[8, j + 1] == b1 || arr[8, j - 1] == b1 || arr[9, j + 1] == b1 || arr[9, j - 1] == b1)
                        {
                            arr[i, j] = num;
                        }
                    }
                    if (arr[i, j] == 0 && j == 0 && i > 0 && i < 9)
                    {
                        if (arr[i, 1] == b1 || arr[i + 1, 1] == b1 || arr[i - 1, 1] == b1 || arr[i + 1, 0] == b1 || arr[i - 1, 0] == b1)
                        {
                            arr[i, j] = num;
                        }
                    }
                    if (arr[i, j] == 0 && j == 9 && i > 0 && i < 9)
                    {
                        if (arr[i, 8] == b1 || arr[i - 1, 8] == b1 || arr[i + 1, 8] == b1 || arr[i + 1, 9] == b1 || arr[i - 1, 9] == b1)
                        {
                            arr[i, j] = num;
                        }
                    }
                }
            }
            if (arr[0, 0] == 0 && (arr[0, 1] == b1 || arr[1, 1] == b1 || arr[1, 0] == b1))
            {
                arr[0, 0] = num;
            }
            if (arr[0, 9] == 0 && (arr[0, 8] == b1 || arr[1, 9] == b1 || arr[1, 8] == b1))
            {
                arr[0, 9] = num;
            }
            if (arr[9, 0] == 0 && (arr[8, 0] == b1 || arr[9, 1] == b1 || arr[8, 1] == b1))
            {
                arr[9, 0] = num;
            }
            if (arr[9, 9] == 0 && (arr[9, 8] == b1 || arr[8, 9] == b1 || arr[8, 8] == b1))
            {
                arr[9, 9] = num;
            }
        } // ממלא את הספרות 2 ליד כל הספרות 1
        public static void Compare(int[,] arr, int[,] arr2)
        {
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    if (arr[i, j] == 1)
                    {
                        arr2[i, j] = arr[i, j];
                    }
                }
            }
        } // מעתיק את הלוח
        public static int[] RandomArr()
        {
            Random rnd = new Random();
            int index = 0;
            int[] tempArr = { 1, 2, 3, 4 };
            int[] arr = new int[4];
            for (int i = tempArr.Length; i > 0; i--)
            {
                int number = rnd.Next(0, i);
                arr[index] = tempArr[number];
                tempArr[number] = 0;
                SetArr(tempArr);
                index++;
            }
            return arr;
        } // הפעולה מחזירה מערך של מספרים בסדר רנדומלי
        public static void SetArr(int[] arr)
        {
            int index = 0;
            int[] newArr = new int[arr.Length];
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] != 0)
                {
                    newArr[index] = arr[i];
                    index++;
                }
            }
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = newArr[i];
            }
        } // הפעולה מסדרת מערך ככה שהתאים הריקים יהיו בסוף
        public static void Ai(int[,] secondArr, int length)
        {
            Random rnd = new Random();
            bool temp1 = true;
            int XstartingPosition = 0;
            int YstartingPosition = 0;
            int[] tempArr = new int[4];
            string direction = "";
            string ans = "no";
            while (ans == "no") // משתנה בסוף
            {
                while (temp1)
                {
                    XstartingPosition = rnd.Next(0, 10);
                    YstartingPosition = rnd.Next(0, 10);
                    if (secondArr[XstartingPosition, YstartingPosition] == 0)
                    {
                        temp1 = false;
                    }
                }
                int index = -1;
                temp1 = true;
                tempArr = RandomArr();
                while (temp1)
                {
                    index++;
                    if (index > 3)
                    {
                        ans = "no";
                        break;
                    }
                    if (tempArr[index] == 1)
                    {
                        direction = "Right";
                        if (CheckZeros(secondArr, XstartingPosition, YstartingPosition, direction, length))
                        {
                            temp1 = false;
                            ans = "yes";
                            tempArr[index] = 0;
                        }
                    }
                    if (tempArr[index] == 2)
                    {
                        direction = "Left";
                        if (CheckZeros(secondArr, XstartingPosition, YstartingPosition, direction, length))
                        {
                            temp1 = false;
                            ans = "yes";
                            tempArr[index] = 0;
                        }
                    }
                    if (tempArr[index] == 3)
                    {
                        direction = "Up";
                        if (CheckZeros(secondArr, XstartingPosition, YstartingPosition, direction, length))
                        {
                            temp1 = false;
                            ans = "yes";
                            tempArr[index] = 0;
                        }
                    }
                    if (tempArr[index] == 4)
                    {
                        direction = "Down";
                        if (CheckZeros(secondArr, XstartingPosition, YstartingPosition, direction, length))
                        {
                            temp1 = false;
                            ans = "yes";
                            tempArr[index] = 0;
                        }
                    }
                }
            }
            if (direction == "Right")
            {
                for (int i = 0; i < length; i++)
                {
                    secondArr[XstartingPosition, YstartingPosition + i] = 1;
                }
            }
            if (direction == "Left")
            {
                for (int i = 0; i < length; i++)
                {
                    secondArr[XstartingPosition, YstartingPosition - i] = 1;
                }
            }
            if (direction == "Up")
            {
                for (int i = 0; i < length; i++)
                {
                    secondArr[XstartingPosition - i, YstartingPosition] = 1;
                }
            }
            if (direction == "Down")
            {
                for (int i = 0; i < length; i++)
                {
                    secondArr[XstartingPosition + i, YstartingPosition] = 1;
                }
            }
        } // המחשב ממקם את הצוללות שלו
        public static string ConvetDirectionToString(ConsoleKeyInfo direction)
        {
            if (direction.Key.Equals(ConsoleKey.RightArrow))
            {
                return "Right";
            }
            if (direction.Key.Equals(ConsoleKey.LeftArrow))
            {
                return "Left";

            }
            if (direction.Key.Equals(ConsoleKey.UpArrow))
            {
                return "Up";

            }
            if (direction.Key.Equals(ConsoleKey.DownArrow))
            {
                return "Down";

            }
            return "";
        } // משנה כיוון למשתנה של סטרינג
        public static bool CheckEndGame(int[,] arr)
        {
            int sum = 0;
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    if (arr[i, j] == 3)
                    {
                        sum++;
                    }
                }
            }
            if (sum == 17)
            {
                return true;
            }
            return false;
        } // הפעולה בודקת אם המשחק נגמר
        public static bool CheckCell(int[,] arr, int x, int y, string direction)
        {
            if (direction == "Right")
            {
                if (y + 1 > 9)
                {
                    return false;
                }
            }
            if (direction == "Left")
            {
                if (y - 1 < 0)
                {
                    return false;
                }
            }
            if (direction == "Up")
            {
                if (x - 1 < 0)
                {
                    return false;
                }
            }
            if (direction == "Down")
            {
                if (x + 1 > 9)
                {
                    return false;
                }
            }
            return true;
        } // הפעולה בודקת אם קיים תא במערך בכיוון הזה      
        public static bool CheckDestroyedShip(int[,] arr, int row, int line)
        {
            int newLine = line;
            int newRow = row;
            if (arr[row, line] == 1)
            {
                return false;
            }
            while (CheckCell(arr, row, line, "Right") && arr[row, line + 1] != 0 && arr[row, line + 1] != 4)
            {
                if (arr[row, line + 1] == 1)
                {
                    return false;
                }
                line++;
            }
            line = newLine;
            while (CheckCell(arr, row, line, "Left") && arr[row, line - 1] != 0 && arr[row, line - 1] != 4)
            {
                if (arr[row, line - 1] == 1)
                {
                    return false;
                }
                line--;
            }
            line = newLine;
            while (CheckCell(arr, row, line, "Up") && arr[row - 1, line] != 0 && arr[row - 1, line] != 4)
            {
                if (arr[row - 1, line] == 1)
                {
                    return false;
                }
                row--;
            }
            row = newRow;
            while (CheckCell(arr, row, line, "Down") && arr[row + 1, line] != 0 && arr[row + 1, line] != 4)
            {
                if (arr[row + 1, line] == 1)
                {
                    return false;
                }
                row++;
            }
            return true;
        } // הפעולה בודקת אם הצוללת התפוצצה או לא
        public static int RandomNumber(int[,] arr)
        {
            int sum = 0;
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    if (arr[i, j] == 0 || arr[i, j] == 1)
                    {
                        sum++;
                    }
                }
            }
            Random rnd = new Random();
            return rnd.Next(0, sum) + 1;
        } // הפעולה נותנ מספר רנדומלי בין התאים הריקים בלוח
        public static bool CheckNumber(string num)
        {
            if (num == "1")
            {
                return true;
            }
            if (num == "2")
            {
                return true;
            }
            if (num == "3")
            {
                return true;
            }
            if (num == "4")
            {
                return true;
            }
            if (num == "5")
            {
                return true;
            }
            if (num == "6")
            {
                return true;
            }
            if (num == "7")
            {
                return true;
            }
            if (num == "8")
            {
                return true;
            }
            if (num == "9")
            {
                return true;
            }
            if (num == "10")
            {
                return true;
            }
            return false;
        }
        public static void Sod(int[,] aiSecondBoard, int[,] playerSecondBoard,int[,] playerBoard, string ans,int computerRow, int computerLine, int count)
        {
            bool loPaga = true;
            if (!CheckEndGame(aiSecondBoard) && loPaga)
            {
                if (ans == "Didnt hit a ship befor")
                {
                    int randomPlace = RandomNumber(playerSecondBoard);
                    for (int i = 0; i < playerSecondBoard.GetLength(0); i++) // המחשב בוחר מיקום  מהמקומת הרקים לזרוק בו פצצה
                    {
                        for (int j = 0; j < playerSecondBoard.GetLength(1); j++)
                        {
                            if (playerSecondBoard[i, j] == 0 || playerSecondBoard[i, j] == 1)
                            {
                                randomPlace--;
                                if (randomPlace == 0)
                                {
                                    computerRow = i;
                                    computerLine = j;
                                    i = playerSecondBoard.GetLength(0);
                                    j = playerSecondBoard.GetLength(1);
                                }
                            }
                        }
                    }
                    if (playerBoard[computerRow, computerLine] == 0) // אם המחשב החטיא
                    {
                        loPaga = true;
                        playerSecondBoard[computerRow, computerLine] = 4;
                        playerBoard[computerRow, computerLine] = 4;
                    }
                    if (playerBoard[computerRow, computerLine] == 1) // אם המחשב פגע
                    {
                        loPaga = false;
                        playerSecondBoard[computerRow, computerLine] = 3;
                        playerBoard[computerRow, computerLine] = 3;
                        ans = "Right";
                        count++;
                    }
                }
                else
                {
                    loPaga = false;
                }
                while (!loPaga)
                {
                    int newComputerRow = computerRow;
                    int newComputerLine = computerLine;
                    if (ans == "Right")
                    {
                        if (CheckCell(playerSecondBoard, newComputerRow, newComputerLine + (count - 1), ans) && playerSecondBoard[newComputerRow, newComputerLine + count] != 4)
                        {
                            newComputerLine += count;
                        }
                        else
                        {
                            ans = "Left";
                            count = 1;
                        }
                    }
                    if (ans == "Left")
                    {
                        if (CheckCell(playerSecondBoard, newComputerRow, newComputerLine - (count - 1), ans) && playerSecondBoard[newComputerRow, newComputerLine - count] != 4)
                        {
                            newComputerLine -= count;
                        }
                        else
                        {
                            ans = "Up";
                            count = 1;
                        }
                    }
                    if (ans == "Up")
                    {
                        if (CheckCell(playerSecondBoard, newComputerRow - (count - 1), newComputerLine, ans) && playerSecondBoard[newComputerRow - count, newComputerLine] != 4)
                        {
                            newComputerRow -= count;
                        }
                        else
                        {
                            ans = "Down";
                            count = 1;
                        }
                    }
                    if (ans == "Down")
                    {
                        if (CheckCell(playerSecondBoard, newComputerRow + (count - 1), newComputerLine, ans) && playerSecondBoard[newComputerRow + count, newComputerLine + count] != 4)
                        {
                            newComputerRow += count;
                        }
                        else
                        {
                            count = 1;
                        }
                    }
                    if (playerBoard[newComputerRow, newComputerLine] == 0) // אם המחשב החטיא
                    {
                        loPaga = true;
                        count = 1;
                        playerSecondBoard[newComputerRow, newComputerLine] = 4;
                        playerBoard[newComputerRow, newComputerLine] = 4;
                        if (ans == "Right")
                        {
                            ans = "Left";
                        }
                        else if (ans == "Left")
                        {
                            ans = "Up";
                        }
                        else if (ans == "Up")
                        {
                            ans = "Down";
                        }
                        else if (ans == "Down")
                        {
                            ans = "Right";
                        }
                    }
                    if (playerBoard[newComputerRow, newComputerLine] == 1) // אם המחשב פגע
                    {
                        loPaga = false;
                        playerSecondBoard[newComputerRow, newComputerLine] = 3;
                        playerBoard[newComputerRow, newComputerLine] = 3;
                        if (CheckDestroyedShip(playerSecondBoard, newComputerRow, newComputerLine))
                        {                           
                            ans = "Didnt hit a ship befor";
                            count = 0;
                            int randomPlace = RandomNumber(playerSecondBoard);
                            for (int i = 0; i < playerSecondBoard.GetLength(0); i++) // המחשב בוחר מיקום  מהמקומת הרקים לזרוק בו פצצה
                            {
                                for (int j = 0; j < playerSecondBoard.GetLength(1); j++)
                                {
                                    if (playerSecondBoard[i, j] == 0 || playerSecondBoard[i, j] == 1)
                                    {
                                        randomPlace--;
                                        if (randomPlace == 0)
                                        {
                                            computerRow = i;
                                            computerLine = j;
                                            i = playerSecondBoard.GetLength(0);
                                            j = playerSecondBoard.GetLength(1);
                                        }
                                    }
                                }
                            }
                            if (playerBoard[computerRow, computerLine] == 0) // אם המחשב החטיא
                            {
                                loPaga = true;
                                playerSecondBoard[computerRow, computerLine] = 4;
                                playerBoard[computerRow, computerLine] = 4;                             
                            }
                            if (playerBoard[computerRow, computerLine] == 1) // אם המחשב פגע
                            {
                                loPaga = false;
                                playerSecondBoard[computerRow, computerLine] = 3;
                                playerBoard[computerRow, computerLine] = 3;                              
                                ans = "Right";
                                count++;
                            }
                        }
                        else
                        {
                            count++;
                        }
                    }
                }
            }
        }
    }
}